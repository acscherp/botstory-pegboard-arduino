int n_row = 0;
int n_col = 0;

void anim_nightrider(CRGB col, uint16_t fade, uint16_t sleep) {

  for (int i=0; i<COLUMNS; i++) {
    for (int j=0; j<ROWS; j++) {
      leds[XY(i,j)].fadeToBlackBy(10);
    }
  }


  leds[XY(n_col,n_row)] = 0xFF0000;
  leds[XY(COLUMNS-n_col,n_row)] = 0xFF0000;

  n_col += 1;
  if (n_col > COLUMNS / 2) {
      n_col = 0;
      n_row += 1;
  }
  if (n_row >= ROWS) {
      n_row = 0;
      n_col = 0;
  }

  FastLED.show();
  delay(sleep);
}
