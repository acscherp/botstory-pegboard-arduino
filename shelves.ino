int cur_shelve = 0;
int shelve_time = 0;

#define NUM_SHELVES 8

int shelves[][2] = {
    {12, 0},
    {0,1},
    {12,4},
    {0,5},
    {12,8},
    {0,9},
    {12,12},
    {0,13}
// 13,0 - 19,0
// 0,1 - 7,1
// 13,4 - 19,4
// 0,5  - 7,5
// 13,8. - 19,8
// 0,9 - 7,9
// 13,12 - 19,12
// 0,14 - 7,14
};

void anim_shelves(CRGB col, uint16_t fade, uint16_t sleep) {

  for (int i=0; i<COLUMNS; i++) {
    for (int j=0; j<ROWS; j++) {
      leds[XY(i,j)].fadeToBlackBy(4);
    }
  }

  int* s = shelves[cur_shelve];
  for (int i=0; i<8; i++) {
      leds[XY(s[0]+i,s[1])] = col;
  }

  shelve_time++;
  if (shelve_time >= fade) {
      shelve_time = 0;
      cur_shelve++;
      if (cur_shelve >= NUM_SHELVES) {
          cur_shelve = 0;
      }
  }

  FastLED.show();
  delay(sleep);
}
