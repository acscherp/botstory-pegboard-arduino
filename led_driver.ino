#include <FastLED.h>

// How many leds in your strip?
#define STRIDE 3
#define LEDS_PER_ROW 60
#define COLUMNS 20
#define ROWS 15
#define NUM_LEDS ROWS * COLUMNS * STRIDE

#define DATA_PIN 27
#define CLOCK_PIN 26

// Define the array of leds
CRGB leds[NUM_LEDS];


char mode;

#define MODE_COMPUTER 'A'
#define MODE_FACEBOOK 'B'
#define MODE_NIGHTRIDER 'C'
#define MODE_PULSE 'D'
#define MODE_SHELVES 'E'

uint32_t mode_color; // 0 = random
int mode_fade; // 0 = random
int mode_delay; // 0 = random

void setup() {
  Serial.begin(19200);
  Serial.println("hello"); // so I can keep track of what is loaded

  FastLED.addLeds<APA102, DATA_PIN, CLOCK_PIN, BGR>(leds, NUM_LEDS);
  LEDS.setBrightness(200);

  mode = MODE_COMPUTER;
  mode_color = (uint32_t) 0xFFFFFF;
  mode_fade = 3;
  mode_delay = 0;

  anim_idle_setup();
}

uint16_t XY( uint8_t x, uint8_t y)
{
  return (y * LEDS_PER_ROW) + (x * STRIDE);
}

uint16_t rXY( uint8_t x, uint8_t y)
{
  return (y * LEDS_PER_ROW) + (x);
}

void loop() {
  CRGB col(mode_color);

  checkSerialCommand();

  switch (mode) {

  case MODE_COMPUTER:

    if (mode_color == 0) {
      hsv2rgb_rainbow(CHSV(random16(0,255),50,100), col);
    }
    anim_idle(col, mode_fade, mode_delay);
    break;

  case MODE_FACEBOOK:
    anim_facebook(col);
    break;

  case MODE_NIGHTRIDER:
    anim_nightrider(col, mode_fade, mode_delay);
    break;

  case MODE_SHELVES:
    anim_shelves(col, mode_fade, mode_delay);
    break;

  case MODE_PULSE:
    anim_pulse(col, mode_fade, mode_delay);
    break;

  }
}

void checkSerialCommand() {
  while (Serial.available()) {
    // delay(3);  //delay to allow buffer to fill

    char ch = Serial.read();
    while (ch == 0xff) {
      ch = Serial.read();
    }
    // ch = Serial.read();

    if (ch == 'X') {
      // set color
      // mode_color = (Serial.read() << 16) ; // & (Serial.read() << 8) & Serial.read();

      char str[7];
      for (int i=0; i<6; i++) {
        str[i] = Serial.read();
      }
      str[6] = 0;
      mode_color = (uint32_t) strtol(str, 0, 16);

      Serial.println(mode_color, HEX);
      //mode_color = 0xFF0000;

      Serial.println("OK");

    } else {
      // set mode
      switch (ch) {
      case MODE_COMPUTER:
      case MODE_PULSE:
        mode = ch;
        mode_delay = Serial.read();
        mode_fade = Serial.read();
        Serial.println("OK");
        break;
      case MODE_FACEBOOK:
      case MODE_NIGHTRIDER:
      case MODE_SHELVES:
        Serial.println("OK");
        mode = ch;
        break;
      default:
        break;
      }
    }
  }
}
