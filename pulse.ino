int pulse = 0;
bool pulseUp = true;

void anim_pulse(CRGB col, uint16_t fade, uint16_t sleep) {
//    uint16_t step = 255 / fade;
    CRGB col2 = CRGB(col.nscale8_video(pulse));
    for (int i=0; i<COLUMNS; i++) {
        for (int j=0; j<ROWS; j++) {
            leds[XY(i,j)] = col2;
        }
    }

    if (pulseUp) {
        if (pulse == 255) {
            pulseUp = false;
        } else {
            pulse += fade;
        }
    } else {
        if (pulse == 10) {
            pulseUp = true;
        } else {
            pulse -= fade;
        }
    }
    pulse = min(255, max(10, pulse));

    FastLED.show();
    delay(sleep);
}
