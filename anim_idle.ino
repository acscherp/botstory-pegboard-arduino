
void anim_idle_setup() {
  leds[XY(0,0)] = CRGB::Green;
}

void anim_idle(CRGB col, uint16_t fade, uint16_t sleep) {

  for (int i=0; i<COLUMNS; i++) {
    for (int j=0; j<ROWS; j++) {
      leds[XY(i,j)].fadeToBlackBy(fade);
    }
  }

  leds[XY(random16(0,COLUMNS), random16(0,ROWS))] = col;

  FastLED.show();
  delay(sleep);
}
