void anim_facebook(CRGB col) {

  for (int i=0; i<COLUMNS; i++) {
    for (int j=0; j<ROWS; j++) {
        leds[XY(i,j)] = col;
    }
  }

  FastLED.show();
  delay(100);
}
